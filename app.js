import http from 'http';
import path from 'path';
const port = 80;
import express from 'express';
import json from 'body-parser';
import { fileURLToPath } from 'url';
import misRutas from './router/index.js';

const _filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(_filename)

const app = express();

// Asignaciones
app.set("view engine", "ejs");
app.use(json.urlencoded({ extended: true }));


app.use(express.static(path.join(__dirname, 'public')));

app.use(misRutas.router);

app.listen(port, () => {
  console.log("Server corriendo en el puerto " + port);
});


